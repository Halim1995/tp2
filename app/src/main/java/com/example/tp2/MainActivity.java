package com.example.tp2;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class MainActivity extends AppCompatActivity {

    BookDbHelper bookDbHelper;
    ListView listView;
    Cursor cursor;
    SimpleCursorAdapter adapter;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listView = (ListView) findViewById(R.id.listView);


        bookDbHelper = new BookDbHelper(getApplicationContext());
        //remplir la BDD
        bookDbHelper.populate();

        cursor = bookDbHelper.fetchAllBooks();
        cursor.moveToFirst();

        //supp un livre
        registerForContextMenu(listView);
        listView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                menu.add(Menu.NONE, 1, Menu.NONE, "Supprimer");


            }
        });


        adapter = new SimpleCursorAdapter(
                        this,
                        android.R.layout.simple_list_item_2,
                        cursor,
                        new String[]{
                                BookDbHelper.COLUMN_BOOK_TITLE,
                                BookDbHelper.COLUMN_AUTHORS
                        },
                        new int[]{android.R.id.text1, android.R.id.text2

                        },
                        0);


        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {

                Intent myIntent = new Intent(v.getContext(), BookActivity.class);
                Cursor cursor = (Cursor) listView.getItemAtPosition(position);
                Book bookSelected = BookDbHelper.cursorToBook(cursor);
                Log.d("opening activity book", bookSelected.toString());
                myIntent.putExtra("bookS", bookSelected);
                startActivity(myIntent);
            }
        });

        // bouton d'ajout
        FloatingActionButton floatbutton = findViewById(R.id.floatbutton);
        floatbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(v.getContext(), BookActivity.class);
                myIntent.putExtra("bookS", (Bundle) null);
                startActivity(myIntent);

            }
        });
    }
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        cursor.moveToPosition(info.position);
        bookDbHelper.deleteBook(cursor);
        cursor = bookDbHelper.fetchAllBooks();
        adapter.changeCursor(cursor);
        adapter.notifyDataSetChanged();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        cursor = bookDbHelper.fetchAllBooks();
        adapter.changeCursor(cursor);
        adapter.notifyDataSetChanged();

    }

}
