package com.example.tp2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class BookActivity extends AppCompatActivity {
    Book book;
    EditText name;
    EditText author;
    EditText pub;
    EditText type;
    EditText publisher;
    Button save;
    BookDbHelper bookDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);
        bookDbHelper = new BookDbHelper(getApplicationContext());
        //selectionner un livre
        Intent intent = getIntent();
        Bundle extras = getIntent().getExtras();

        book = (Book) extras.get("bookS");

        name = (EditText) findViewById(R.id.nameBook) ;
        author = (EditText) findViewById(R.id.editAuthors) ;
        pub = (EditText) findViewById(R.id.editYear) ;
        type = (EditText) findViewById(R.id.editGenres) ;
        publisher = (EditText) findViewById(R.id.editPublisher) ;

        if(book!=null) {
            name.setText(book.getTitle());
            author.setText(book.getAuthors());
            pub.setText(book.getYear());
            type.setText(book.getGenres());
            publisher.setText(book.getPublisher());
        }

        save = (Button) findViewById(R.id.save);

        //sauvegarde
        Button save = findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                System.out.println("saving");

                if(book!=null) {
                    if(!name.getText().toString().isEmpty()) {
                        book.setTitle(name.getText().toString());
                        book.setAuthors(author.getText().toString());
                        book.setYear(pub.getText().toString());
                        book.setGenres(type.getText().toString());
                        book.setPublisher(publisher.getText().toString());
                        bookDbHelper.updateBook(book);

                        Toast.makeText(getApplicationContext(),"la modification a bien été effectuée ",Toast.LENGTH_LONG).show();
                    }

                    else {
                        System.out.print("titre vide");
                        AlertDialog.Builder builder = new AlertDialog.Builder(BookActivity.this);
                        builder.setMessage("Le titre du livre doit etre non vide.");
                        builder.setCancelable(true);
                        builder.show();
                        return;
                    }
                }
                else {
                    if(name.getText().toString().isEmpty()){
                        System.out.print("titre vide");
                        AlertDialog.Builder builder = new AlertDialog.Builder(BookActivity.this);
                        builder.setMessage("Le titre du livre doit etre non vide.");
                        builder.setCancelable(true);
                        builder.show();
                        return;
                    }
                    else {
                        book = new Book(name.getText().toString(), author.getText().toString(), pub.getText().toString(), type.getText().toString(), publisher.getText().toString());

                        if(!bookDbHelper.addBook(book)){
                            System.out.print("meme nom");
                            AlertDialog.Builder builder = new AlertDialog.Builder(BookActivity.this);
                            builder.setMessage("Un livre portant le meme nom existe déjà dans la base de données");
                            builder.setCancelable(true);
                            builder.show();
                            return;

                        }
                        else
                            Toast.makeText(getApplicationContext(),"La modification a bien été effectuée ",Toast.LENGTH_LONG).show();


                    }

                }


            }
        });
    }
}
